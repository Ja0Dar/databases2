package controllers;

import datamodel.*;
import forms.*;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Application extends Controller {
    @Inject
    FormFactory formFactory;

    public Result index() {

        Form<ProductFields> productForm = formFactory.form(ProductFields.class);
        Form<SupplierFields> supplierForm = formFactory.form(SupplierFields.class);
        Form<OrderFields> orderForm = formFactory.form(OrderFields.class);
        Form<CategoryFields> categoryForm = formFactory.form(CategoryFields.class);

        Optional<Persistable> validForm = Stream.of(productForm, supplierForm, orderForm, categoryForm)
                .map(Form::bindFromRequest)
                .filter(form -> !form.hasErrors())
                .map(form -> (Persistable) form.get())
                .findFirst();

        Logger.debug("index ".concat(validForm.isPresent() ? "Form recognised" : "Get without form recofnised"));

        if (validForm.isPresent()) {
            try (Session s = SessionProvider.openSession()) {
                Logger.error(validForm.get().toString());
                boolean persisted = validForm.get().persistWithTransaction(s);

                if(persisted)
                    return redirect("/");
                else return badRequest(main.render("Couldnt save","Couldn't save resource, check parameters supplied"));
            }catch (ConstraintViolationException ex){
                return badRequest(main.render("Couldnt save","Resource already exists"));
            }
        }

        Session s = SessionProvider.openSession();

        List<Order> orderList = s.createQuery("from Order", Order.class).list();
        List<Supplier> supplierList = s.createQuery("from Supplier", Supplier.class).list();
        List<Category> categoryList = s.createQuery("from Category", Category.class).list();
        s.close();

        return ok(index.render(categoryList, supplierList, orderList));
    }


    public Result categoryResult(Integer id) {
        try (Session s = SessionProvider.openSession()) {
            Category cat = s.get(Category.class, id);
            if (cat == null)
                return notFound();
            return ok(category.render(cat));
        }
    }

    public Result orderResult(Integer id) {
        try (Session s = SessionProvider.openSession()) {
            Order ord = s.get(Order.class, id);
            if (ord == null)
                return notFound();
            return ok(order.render(ord));
        }
    }

    public Result supplierResult(String id) {
        try (Session s = SessionProvider.openSession()) {
            Supplier sup = s.get(Supplier.class, id);
            if (sup == null)
                return notFound();
            sup.getProducts().toArray();
            return ok(supplier.render(sup));
        }
    }

    public Result productResult(Integer id) {
        try (Session s = SessionProvider.openSession()) {
            Product prod = s.get(Product.class, id);

            Category cat = s.get(Category.class, prod.getCategoryId());

            List<Order> orderList = s.createQuery("from Order", Order.class).getResultList();


            return ok(product.render(prod, cat, orderList));
        } catch (NullPointerException ex) {
            return badRequest();
        }


    }
}

