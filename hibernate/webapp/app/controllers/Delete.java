package controllers;

import datamodel.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

public class Delete extends Controller {


    public Result deleteCategory(Integer id) {
        try (Session s = SessionProvider.openSession()) {
            Transaction tx = s.beginTransaction();
            Category category = s.get(Category.class, id);
            s.delete(category);
            tx.commit();
            return noContent();
        }
    }

    public Result deleteProduct(Integer id) {
        try (Session s = SessionProvider.openSession()) {
            Transaction tx = s.beginTransaction();
            Product prod = s.get(Product.class, id);
            s.delete(prod);
            tx.commit();
            return noContent();
        }
    }

    public Result deleteOrder(Integer id) {
        try (Session s = SessionProvider.openSession()) {
            Transaction tx = s.beginTransaction();
            Order ord = s.get(Order.class, id);
            s.delete(ord);
            tx.commit();
            return noContent();
        }
    }

    public Result deleteSupplier(String companyName) {
        try (Session s = SessionProvider.openSession()) {
            Transaction tx = s.beginTransaction();
            Supplier ord = s.get(Supplier.class, companyName);
            s.delete(ord);
            tx.commit();
            return noContent();
        }
    }

    public Result deleteProductOrder(Integer productId, Integer orderId) {
        try (Session s = SessionProvider.openSession()) {
            Transaction tx = s.beginTransaction();
            s.createQuery("delete from ProductOrder  where product.id=:productId and order.id=:orderId")
                    .setParameter("productId", productId)
                    .setParameter("orderId", orderId).executeUpdate();
            return noContent();
        }
    }
}

