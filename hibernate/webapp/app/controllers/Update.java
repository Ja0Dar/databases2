package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import datamodel.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class Update extends Controller {

    public Result addProductOrder() {
        Http.Request d = request();
        String x = d.body().asText();
        try (Session s = SessionProvider.openSession()) {
            JsonNode json = request().body().asJson();
            int orderid= json.get("orderId").asInt();
            int prodId = json.get("productId").asInt();
            int quantity = json.get("quantity").asInt();
            Product prod = s.get(Product.class,prodId);
            Order ord = s.get(Order.class,orderid);


            Transaction tx = s.beginTransaction();
            if(prod.getUnitsOnStock()>=quantity){
                ProductOrder po = new ProductOrder(quantity,prod,ord);
                s.persist(po);
                tx.commit();

            }else {
                tx.rollback();
                return badRequest();
            }
            return created();
        }catch (NullPointerException ex){
            return badRequest();
        }
    }
}

