package datamodel;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

    String street;
    String city;
    String zipCode;

    public Address(final String street, final String city, final String zipCode) {

        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
    }

    public Address() {
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(final String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }


    @Override
    public String toString() {
        return String.format("Address(street : %s, city : %s,zipCode: %s)", street, city, zipCode);
    }
}
