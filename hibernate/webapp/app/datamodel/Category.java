package datamodel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Category {
    @Id
    @GeneratedValue
    int categoryId;

    @Column(nullable = false)
    String name;

    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "productCategoryId")
    List<Product> products;

    void addProduct(Product p) {
        this.products.add(p);
        p.categoryId = categoryId;
    }

    public Category() {
        this.products= new ArrayList<>();
    }

    public Category(final String name) {
        this.name = name;
        this.products= new ArrayList<>();
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(final int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }
}
