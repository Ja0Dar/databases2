package datamodel;

import javax.persistence.Entity;

@Entity
public class Customer extends Company {
    float discount;

    public Customer() {
    }

    public Customer(final String companyName, final Address address, final float discount) {
        super(companyName, address);
        this.discount = discount;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(final float discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return String.format("Customer(%s,%s,%f", companyName,address.toString(),discount);
    }
}
