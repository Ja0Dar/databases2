package datamodel;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "ShopOrder")
public class Order {
    @Id
    @GeneratedValue
    int orderId;

    @OneToMany(mappedBy = "order",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    List<ProductOrder> products;

    //*/
    public Order() {
        this.products = new ArrayList<>();
    }

    public Order(final Map<Product, Integer> products) {

        this.products = new ArrayList<>();
        if (products != null)
            products.forEach((product, quantity) ->
                    ProductOrder.addRelation(product, this, quantity)
            );
    }



    public boolean addProduct(Product product, int quantity) {
        return ProductOrder.addRelation(product,this,quantity);
    }



    public List<ProductOrder>  getProductOrders() {
        return ImmutableList.copyOf(products);
    }

    public int getOrderId() {
        return orderId;
    }
}
