package datamodel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Product {

    @Id
    @GeneratedValue
    int productId;
    String name;

    public Integer getProductId() {
        return productId;
    }

    private int unitsOnStock;

    @ManyToOne(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @JoinColumn()
    Supplier supplier;

    @Column(name = "productCategoryId")
    int categoryId;


    @OneToMany(mappedBy = "product", cascade = {CascadeType.PERSIST, CascadeType.REMOVE},fetch = FetchType.EAGER)
    List<ProductOrder> orders;

    public Product() {
        this.orders = new ArrayList<>();
    }


    public Product(String name, int unitsOnStock, int categoryId, Supplier supplier) {
        this(name, unitsOnStock, categoryId, supplier, new HashMap<>());
    }

    public Product(String name, int unitsOnStock, int categoryId, Supplier supplier, Map<Order, Integer> orders) {
        this.categoryId = categoryId;
        this.name = name;
        this.unitsOnStock = unitsOnStock;
        setSupplier(supplier);
        this.orders = new ArrayList<>();

        orders.forEach((shopOrder, quantity) -> ProductOrder.addRelation(this, shopOrder, quantity));

    }

    public boolean addToOrder(Order order, Integer quantity) {
        return ProductOrder.addRelation(this, order, quantity);
    }

    public String getName() {
        return name;
    }

    public int getCategoryId() {
        return categoryId;
    }


    private int unitsSold() {

        int i=3;
        return orders.stream().map(ProductOrder::getQuantity).reduce(Integer::sum).orElse(0);
    }

    public void setName(final String productName) {
        this.name = productName;
    }

    public int getUnitsOnStock() {
        return unitsOnStock - unitsSold();
    }

    public void setUnitsOnStock(final int unitsOnStock) {
        this.unitsOnStock = unitsOnStock + unitsSold();
    }

    public Supplier getSupplier() {
        return supplier;

    }

    public void setSupplier(final Supplier supplier) {
        if (this.supplier == supplier)
            return;


        if (this.supplier != null)
            this.supplier.products.remove(this);

        if (supplier != null)
            supplier.products.add(this);

        this.supplier = supplier;
    }

    public List<ProductOrder> getProductOrders() {
        return orders;
    }

}
