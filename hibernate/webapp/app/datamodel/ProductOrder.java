package datamodel;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints =
@UniqueConstraint(columnNames = {"productFK", "orderFK"})
)
public class ProductOrder {
    @Id
    @GeneratedValue
    int id;

    int quantity;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "productFK")
    Product product;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "orderFK")
    Order order;


    public ProductOrder(final Integer quantity, final Product product, final Order order) {
        this.quantity = quantity;
        this.product = product;
        product.orders.add(this);
        this.order = order;
        order.products.add(this);
    }

    public ProductOrder() {
    }

    public static boolean addRelation(Product product, Order order, int quantity) {
        if (product.getUnitsOnStock() >= quantity) {
            new ProductOrder(quantity, product, order);
            return true;
        }
        return false;
    }


    public boolean raiseQuantity(int delta) {
        if (product.getUnitsOnStock() >= delta) {
            quantity += delta;
            return true;
        }
        return false;
    }

    public boolean decreaseQuantity(int delta) {
        if (quantity > delta) {
            quantity -= delta;
            return true;
        } else if (quantity == delta) {
            order.products.remove(this);
            product.orders.remove(this);
            return true;
        }
        return false;
    }


    public void unlink() {
        order.products.remove(this);
        product.orders.remove(this);
    }

    public int getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public String toString() {
        String productStr = product == null ? "null" : product.getName();
        String orderStr = order == null ? "null" : String.format("orderResult(%d)", order.getOrderId());

        return String.format("ProductOrder(%s,%s,%d)", productStr, orderStr, quantity);
    }
}
