package datamodel;

import com.google.common.collect.ImmutableList;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Supplier extends Company {
    long bankAccountNumber;

    @OneToMany(mappedBy = "supplier",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    List<Product> products;


    public Supplier() {
    }

    public Supplier(final String companyName, final Address address,long bankAccountNumber) {
        this.companyName = companyName;
        this.address = address;
        this.bankAccountNumber = bankAccountNumber;
        products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return ImmutableList.copyOf(products);
    }

    public void setProducts(final List<Product> products) {
        this.products.forEach(product -> product.supplier = null);
        if (products == null) {
            this.products = new ArrayList<>();
        } else {
            products.forEach(product -> product.supplier = this);
            this.products = products;
        }
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(final Address address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

    public long getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(final long bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Override
    public String toString() {
        return String.format("Supplier(%s,%s,%d)", companyName,address.toString(),bankAccountNumber);
    }
}
