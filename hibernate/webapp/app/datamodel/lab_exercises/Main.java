package datamodel.lab_exercises;

import datamodel.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
//Code comented, because datamodel was changing through exercises and code wasn't compiling
    static private SessionFactory sessionFactory = null;

    static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            sessionFactory = configuration.configure().buildSessionFactory();
        }
        return sessionFactory;
    }

//    static void save(Product productResult) {
//        Session session = getEMF().openSession();
//        Transaction tx = session.beginTransaction();
//        session.save(productResult);
//        if (productResult.getSupplier() != null) {
//            session.save(productResult.getSupplier());
//        }
//        tx.commit();
//        session.close();
//    }


    public static void ex2() throws IOException {
//        Product p = new Product("milk", 10);
//        save(p);

//        Session session = getEMF().openSession();
//        Product result = session.get(Product.class, "milk");
//        session.close();
//
//        if (result.getUnits() == p.getUnits()) {
//            System.out.println(String.format("Save and get %s.", result.getUnits() == p.getUnits() ? "succeeded" : "failed"));
//        }
    }

    public static void ex3() throws IOException {
//        Supplier s = new Supplier("MilkyMilk", "Sesame Street", "City");
//
//        Session session = getEMF().openSession();
//        Transaction tx = session.beginTransaction();
//        Product milk = session.get(Product.class, "milk");
//        session.save(s);
//        milk.setSupplier(s);
//        tx.commit();
//        session.close();
    }

    /*
        public static void ex4_5() {

            List<Product> products = Arrays.asList(new Product("milk", 10),
                    new Product("cheese", 11),
                    new Product("yogurt", 12),
                    new Product("eggs", 18));

            Supplier supplierResult = new Supplier("MilkyMilk", "Sesame Street", "City");

            Session session = getEMF().openSession();
            Transaction tx = session.beginTransaction();
            session.save(supplierResult);
            for (Product productResult : products) {
                session.save(productResult);
            }
            supplierResult.setProducts(products);
            tx.commit();
            session.close();
        }

        public static void ex6() {
            Supplier s1 = new Supplier("MilkyMilk", "Sesame Street", "City");
            Supplier s2 = new Supplier("BeefyBeef", "Diagon Alley", "London");

            List<Product> beefy = Arrays.asList(new Product("Dragon Ham", 1400),
                    new Product("Regular Ham", 3));

            List<Product> milkProducts = Arrays.asList(new Product("milk", 10),
                    new Product("cheese", 11),
                    new Product("yogurt", 12));


            Session session = getEMF().openSession();
            Transaction tx = session.beginTransaction();

    //        milky
            session.save(s1);
            milkProducts.forEach(session::save);
            s1.setProducts(milkProducts);

            //beefy
            session.save(s2);
            beefy.forEach(session::save);
            s2.setProducts(beefy);

            tx.commit();
            session.close();


            session = getEMF().openSession();
            Supplier resultSupplier = session.get(Supplier.class, "BeefyBeef");
            System.out.println("Products of BeefyBeef");
            resultSupplier.getProducts().forEach(productResult -> System.out.println(productResult.getName()));

            Product yogurt = session.get(Product.class, "yogurt");
            System.out.println(String.format("Yogurt supplierResult: %s ", yogurt.getSupplier().getCompanyName()));
            session.close();
        }

    */
    public static void ex8() {
  /*      List<Product> beefy = Arrays.asList(new Product("Dragon Ham", 1400),
                new Product("Regular Ham", 3));

        List<Product> milky = Arrays.asList(new Product("milk", 10),
                new Product("cheese", 11),
                new Product("yogurt", 12));

        Order o1 = new Order(beefy.stream().collect(Collectors.toMap(productResult -> productResult, productResult -> 1)));
        Order o2 = new Order(milky.stream().collect(Collectors.toMap(productResult -> productResult, productResult -> 1)));

        Session session = getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        //beefy
        session.save(o1);
        beefy.forEach(session::save);
//        milky
        session.save(o2);
        milky.forEach(session::save);

        o1.addProduct(milky.get(0),1); // milk

        Stream.of(o1,o2).
                flatMap(shopOrder -> shopOrder.getProductOrders().stream())
                .forEach(session::save);

        tx.commit();
        session.close();

///*
        session = getSessionFactory().openSession();
        Order trans = session.get(Order.class, o1.getOrderId());

        System.out.println(String.format("Products in Order: %d", trans.getOrderId()));
        trans.getProductOrders().forEach(System.out::println);

        System.out.println("Milk orders:");
        session.get(Product.class,milky.get(0).getProductId())//milk
                .getProductOrders().stream()
                .map(ProductOrder::getOrder)
                .map(Order::getOrderId)
                .forEach(System.out::println);

        session.close();*/
    }

//    public static void ex10() {
//        List<Product> beefy = Arrays.asList(new Product("Dragon Ham", 1400),
//                new Product("Regular Ham", 3));
//
//        List<Product> milky = Arrays.asList(new Product("milk", 10),
//                new Product("cheese", 11),
//                new Product("yogurt", 12));
//
//        Order o1 = new Order(beefy.stream().collect(Collectors.toMap(productResult -> productResult, productResult -> 1)));
//        Order o2 = new Order(milky.stream().collect(Collectors.toMap(productResult -> productResult, productResult -> 1)));
//
//        Session session = getSessionFactory().openSession();
//        Transaction tx = session.beginTransaction();
//
//        session.persist(o1);
//        session.persist(o2);
//        o1.addProduct(milky.get(0),1); // milk
//
//        tx.commit();
//        session.close();
//
//        session = getSessionFactory().openSession();
//        Order trans = session.get(Order.class, o1.getOrderId());
//
//        System.out.println(String.format("Products in Order: %d", trans.getOrderId()));
//        trans.getProductOrders().forEach(System.out::println);
//
//        System.out.println("Milk orders:");
//        session.get(Product.class,milky.get(0).getProductId())//milk
//                .getProductOrders().stream()
//                .map(ProductOrder::getOrder)
//                .map(Order::getOrderId)
//                .forEach(System.out::println);
//
//        session.close();
//    }


    public static void ex11() {



        Session session = getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        Supplier supplier = new Supplier("MilkyMilk",new Address("Sesame Street", "City","zippyZipcode"),123);
        session.persist(supplier);

        tx.commit();
        session.close();

        session = getSessionFactory().openSession();
        Supplier fromDbSupplier= session.get(Supplier.class, "MilkyMilk");

        session.close();
        System.out.println(fromDbSupplier.getAddress());

    }

    public static void ex12() {



        Session session = getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        Supplier supplier = new Supplier("MilkyMilk",new Address("Sesame Street", "City"," DC 20521-9000"),1234);
        session.persist(supplier);
        Customer cust = new Customer("Waldemart",new Address("every street","every city","every zipcode"),0.3f);

        session.persist(cust);

        tx.commit();
        session.close();

        session = getSessionFactory().openSession();
        Supplier fromDbSupplier= session.get(Supplier.class, "MilkyMilk");
        Customer fromDbCust= session.get(Customer.class, "Waldemart");

        session.close();
        System.out.println(fromDbSupplier.toString());
        System.out.println(fromDbCust.toString());

    }
    public static void main(String[] args) throws IOException {
        // To not drop table on session factory creation : use "update" instead of create. Then beware of PK duplicate


//        ex2();
//        ex3();


//        ex4a_5();
//        ex6();


        ex8();



//        ex10();



//        ex12();
    }


}
