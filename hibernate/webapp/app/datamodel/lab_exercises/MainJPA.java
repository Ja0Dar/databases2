package datamodel.lab_exercises;

import datamodel.Order;
import datamodel.Product;
import datamodel.ProductOrder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainJPA {

    //Code comented, because datamodel was changing through exercises and code wasn't compiling
    static private EntityManagerFactory emf = null;

    static EntityManagerFactory getEMF() {
        if (emf== null) {
            emf= Persistence.createEntityManagerFactory("JPA_EM");
        }
        return emf;
    }

    public static void ex9() {
//    List<Product> beefy = Arrays.asList(new Product("Dragon Ham", 1400),
//            new Product("Regular Ham", 3));
//
//    List<Product> milky = Arrays.asList(new Product("milk", 10),
//            new Product("cheese", 11),
//            new Product("yogurt", 12));
//
//    Order o1 = new Order(beefy.stream().collect(Collectors.toMap(productResult -> productResult, productResult -> 1)));
//    Order o2 = new Order(milky.stream().collect(Collectors.toMap(productResult -> productResult, productResult -> 1)));
//
//    EntityManager entityManager= getEMF().createEntityManager();
//    EntityTransaction tx = entityManager.getTransaction();
//    tx.begin();
//
    //beefy
//    entityManager.persist(o1);
//    beefy.forEach(entityManager::persist);
//        milky
//    entityManager.persist(o2);
//    milky.forEach(entityManager::persist);
//
//    o1.addProduct(milky.get(0),1); // milk
//
//    Stream.of(o1,o2).
//            flatMap(shopOrder -> shopOrder.getProductOrders().stream())
//            .forEach(entityManager::persist);
//
//    tx.commit();
//    entityManager.close();

//    entityManager = getEMF().createEntityManager();
//    Order trans = entityManager.find(Order.class, o1.getOrderId());

//    System.out.println(String.format("Products in Order: %d", trans.getOrderId()));
//    trans.getProductOrders().forEach(System.out::println);

//    System.out.println("Milk orders:");
//    entityManager.find(Product.class,milky.get(0).getProductId())//milk
//            .getProductOrders().stream()
//            .map(ProductOrder::getOrder)
//            .map(Order::getOrderId)
//            .forEach(System.out::println);

//    entityManager.close();
}

    public static void main(String[] args) {
        ex9();
    }
}
