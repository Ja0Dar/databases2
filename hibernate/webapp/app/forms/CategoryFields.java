package forms;

import datamodel.Category;
import org.hibernate.Session;
import org.hibernate.Transaction;
import play.data.validation.Constraints;

public class CategoryFields implements ToDataModel<Category> ,Persistable{
    @Override
    public boolean persistWithTransaction(final Session s) {
        Transaction tx = s.beginTransaction();
        s.persist(toDataModel());
        tx.commit();
        return true;
    }

    @Constraints.Required
    String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(final String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public Category toDataModel() {
        return new Category(categoryName);
    }
}
