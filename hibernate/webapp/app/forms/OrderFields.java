package forms;

import datamodel.Order;
import org.hibernate.Session;
import org.hibernate.Transaction;
import play.data.validation.Constraints;

public class OrderFields implements ToDataModel<Order>, Persistable{
    public Integer getOrderFormIndicator() {
        return orderFormIndicator;
    }

    public void setOrderFormIndicator(final Integer orderFormIndicator) {
        this.orderFormIndicator = orderFormIndicator;
    }

    @Constraints.Required
    Integer orderFormIndicator;


    @Override
    public Order toDataModel() {
        return new Order();
    }

    public boolean persistWithTransaction(final Session s) {
        Transaction tx = s.beginTransaction();
        s.persist(toDataModel());
        tx.commit();
        return true;
    }
}
