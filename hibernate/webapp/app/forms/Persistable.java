package forms;

import org.hibernate.Session;

public interface Persistable {
    boolean persistWithTransaction(Session s);
}
