package forms;

import datamodel.Product;
import datamodel.Supplier;
import org.hibernate.Session;
import org.hibernate.Transaction;
import play.data.validation.Constraints;

public class ProductFields implements Persistable {
    @Constraints.Required
    String name;

    @Constraints.Required
    int units;

    @Constraints.Required
    String supplierName;

    @Constraints.Required
    Integer categoryId;



    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(final int units) {
        this.units = units;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(final String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(final Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean persistWithTransaction(final Session s) {
        if(units<=0){
            return false;
        }
        Transaction tx= s.beginTransaction();
        Supplier sup = s.get(Supplier.class,supplierName);
        Product prod =new Product(name, units,categoryId,sup);
        s.persist(prod);
        tx.commit();
        return true;
    }
}
