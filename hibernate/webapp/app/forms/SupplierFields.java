package forms;

import datamodel.Address;
import datamodel.Supplier;
import org.hibernate.Session;
import org.hibernate.Transaction;
import play.data.validation.Constraints;

public class SupplierFields implements ToDataModel<Supplier>, Persistable {
    @Constraints.Required
    String companyName;
    @Constraints.Required
    String city;
    @Constraints.Required
    String zipCode;
    @Constraints.Required
    String street;
    @Constraints.Required
    long accountNumber;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(final String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(final long accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean persistWithTransaction(final Session s) {
        Transaction tx = s.beginTransaction();
        Supplier other = s.get(Supplier.class, companyName);
        if (other == null) {
            s.persist(toDataModel());
            tx.commit();
            return true;
        }
        tx.rollback();
        return false;
    }

    @Override
    public Supplier toDataModel() {
        return new Supplier(companyName, new Address(street, city, zipCode), accountNumber);
    }
}
