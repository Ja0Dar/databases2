package forms;

public interface ToDataModel <T>{
    T toDataModel();
}
