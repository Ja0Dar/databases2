name := "webapp"

version := "1.0"

lazy val `webapp` = (project in file(".")).enablePlugins(PlayJava)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += JCenterRepository
resolvers += JavaNet2Repository

scalaVersion := "2.11.11"

val derbyVersion = "10.13.1.1"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "org.hibernate" % "hibernate-core" % "5.2.10.Final",
  "org.hsqldb" % "hsqldb" % "2.2.9",
  "org.slf4j" % "slf4j-api" % "1.7.5",

//  "log4j" % "log4j" % "1.2.17",
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "org.apache.derby" % "derby" % derbyVersion % Test,
  "org.apache.derby" % "derbytools" % derbyVersion,
  "org.apache.derby" % "derbyclient" % derbyVersion,
  "org.apache.derby" % "derbynet" % derbyVersion,

  "junit" % "junit" % "4.11" % Test,
  "org.easytesting" % "fest-assert" % "1.4" % Test


)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

      