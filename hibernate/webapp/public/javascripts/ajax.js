function addOrder(productId,successCallback,failureCallback) {
    $.ajax({
        url: '/productorder',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: 'application/json',
        data: JSON.stringify({
            "productId": productId,
            "orderId": document.getElementById('order').value,
            "quantity": document.getElementById('quantity').value
        }),
        success: function () {
            location.reload();
        },
        error: function () {
            location.reload();
        }
    });
}