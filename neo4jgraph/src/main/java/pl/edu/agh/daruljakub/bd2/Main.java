package pl.edu.agh.daruljakub.bd2;

import pl.edu.agh.daruljakub.bd2.dao.DAO;
import pl.edu.agh.daruljakub.bd2.dao.HeroDAO;
import pl.edu.agh.daruljakub.bd2.dao.Labels;
import pl.edu.agh.daruljakub.bd2.generator.AilmentNames;
import pl.edu.agh.daruljakub.bd2.generator.Generator;
import pl.edu.agh.daruljakub.bd2.generator.HeroNames;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        //do not run on populated db.
        Generator.generate();

        DAO.printStats();
        HeroDAO hd = new HeroDAO();

        System.out.println(hd.findPathBetween(HeroNames.piglet.toHero(), AilmentNames.adhd.toAilment(),Labels.Ailment));
        System.out.println(hd.getRelationshipsFor(HeroNames.pooh.toHero()));


    }
}
