package pl.edu.agh.daruljakub.bd2.dao;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import pl.edu.agh.daruljakub.bd2.model.Ailment;

public class AilmentDAO extends DAO<Ailment> {
    static final String SHORT_NAME_COLUMN = "short_name";

    public AilmentDAO() {
        super(Labels.Ailment);
    }


    public void addAilment(Ailment a) {

        try (Transaction tr = db.getDBService().beginTx()) {

            Node n = createNode();
            n.setProperty(NAME_COLUMN, a.getName());
            if (a.getShortName().isPresent())
                n.setProperty(SHORT_NAME_COLUMN, a.getShortName().get());
            tr.success();
        }
    }

}
