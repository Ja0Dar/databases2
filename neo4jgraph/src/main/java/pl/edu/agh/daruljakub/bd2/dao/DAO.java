package pl.edu.agh.daruljakub.bd2.dao;

import org.neo4j.graphdb.Node;
import pl.edu.agh.daruljakub.bd2.model.Named;

public abstract class DAO<T extends Named> {
    public static final GraphDatabase db = GraphDatabase.createDatabase();
    static final String NAME_COLUMN = "name";
    Labels label;


    DAO(Labels label) {
        this.label = label;
    }

    Node createNode() {
        return db.getDBService().createNode(label);
    }

    Node createNodeWithName(T t) {
        Node n = db.getDBService().createNode(label);
        n.setProperty(NAME_COLUMN, t.getName());
        return n;
    }

    Node findNode(T t) {
        return db.getDBService().findNode(label, NAME_COLUMN, t.getName());
    }


    public String getRelationshipsFor(T t) {
        return db.runCypher(String.format("Match (n:%s{%s:'%s'})-[r]-(n2) Return n, r, n2",
                label,
                NAME_COLUMN,
                t.getName()
        ));

    }

    public <A extends Named> String findPathBetween(T source, A destination, Labels destLabel) {
        return db.runCypher(String.format("Match (n:%s{%s:'%s'}),(n2:%s{%s:'%s'}),p= shortestpath((n)-[*]-(n2)) Return p",
                label,
                NAME_COLUMN,
                source.getName(),
                destLabel,
                NAME_COLUMN,
                destination.getName()
        ));
    }

    public static void printStats() {

        System.out.println(db.runCypher("CALL db.labels()"));
        System.out.println(db.runCypher("CALL db.relationshipTypes()"));
    }
}
