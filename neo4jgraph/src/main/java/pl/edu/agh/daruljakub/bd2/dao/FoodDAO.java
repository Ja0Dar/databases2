package pl.edu.agh.daruljakub.bd2.dao;

import org.neo4j.graphdb.Transaction;
import pl.edu.agh.daruljakub.bd2.model.Food;

public class FoodDAO extends DAO<Food> {

    public FoodDAO() {
        super(Labels.Food);
    }

    public void addFood(Food f) {
        try (Transaction tr = db.getDBService().beginTx()) {
            createNodeWithName(f);
            tr.success();
        }
    }
}
