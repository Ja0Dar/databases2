package pl.edu.agh.daruljakub.bd2.dao;


import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import pl.edu.agh.daruljakub.bd2.model.Ailment;
import pl.edu.agh.daruljakub.bd2.model.Hero;
import pl.edu.agh.daruljakub.bd2.model.Food;

public class HeroDAO extends DAO<Hero> {
    static final String BIOCLASS_PROPERTY = "bio_class";

    public HeroDAO() {
        super(Labels.Hero);
    }


    public void addHero(Hero h) {
        try (Transaction tr = db.getDBService().beginTx()) {
            Node n = createNodeWithName(h);
            if (h.getBioClass().isPresent())
                n.setProperty(BIOCLASS_PROPERTY, h.getBioClass().get().toString());
            tr.success();
        }
    }

    public void suffersFrom(Hero h, Ailment a) {

        try (Transaction tr = db.getDBService().beginTx()) {
            Node heroNode = findNode(h);
            Node ailmentNode = new AilmentDAO().findNode(a);
            heroNode.createRelationshipTo(ailmentNode, Relationships.SUFFERS_FROM);
            tr.success();
        }
    }

    public void caresFor(Hero h, Hero carerdFor) {

        try (Transaction tr = db.getDBService().beginTx()) {
            Node heroNode = findNode(h);
            Node caredForNode = new HeroDAO().findNode(carerdFor);
            heroNode.createRelationshipTo(caredForNode, Relationships.CARES_FOR);
            tr.success();
        }
    }


    public void eats(Hero h, Food f) {
        try (Transaction tr = db.getDBService().beginTx()) {
            Node heroNode = findNode(h);
            Node foodNode = new FoodDAO().findNode(f);
            heroNode.createRelationshipTo(foodNode, Relationships.EATS);
            tr.success();
        }

    }


}
