package pl.edu.agh.daruljakub.bd2.dao;

import org.neo4j.graphdb.RelationshipType;

public enum Relationships implements RelationshipType {
    EATS,SUFFERS_FROM,CARES_FOR,
}
