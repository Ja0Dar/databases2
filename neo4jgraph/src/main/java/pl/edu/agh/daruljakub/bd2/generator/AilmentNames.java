package pl.edu.agh.daruljakub.bd2.generator;

import pl.edu.agh.daruljakub.bd2.model.Ailment;

import java.util.Optional;

public enum AilmentNames {
    adhd("Attention deficit hyperactivity disorder", "ADHD"),
    ocd("Obsessive compulsive disorder", "OCD"),
    anxiety("Generalized anxiety disorder", "GAD"),
    dyslexia("Dyslexia"),
    socialAnxiety("Social anxiety disorder", "SAD"),
    depression("Depressive disorder"),
    schizophrenia("Schizophrenia");


    public String getName() {
        return name;
    }

    public Optional<String> getShortName() {
        return shortName;
    }

    private String name;
    private Optional<String> shortName;


    AilmentNames(String name) {
        this.name = name;
        shortName = Optional.empty();
    }

    AilmentNames(String name, String shortName) {
        this.name = name;
        this.shortName = Optional.of(shortName);
    }

    public Ailment toAilment() {
        return shortName.map(s -> new Ailment(name, s)).orElseGet(() -> new Ailment(name));
    }
}
