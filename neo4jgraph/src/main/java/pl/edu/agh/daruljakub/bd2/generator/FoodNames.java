package pl.edu.agh.daruljakub.bd2.generator;

import pl.edu.agh.daruljakub.bd2.model.Food;

import java.util.Optional;

public enum FoodNames {

    honey,
    acorn,
    thistle,
    carrot,
    birthdayCake("birthday cake"),
    maltExtract("malt extract"),
    porridge;

    private final Optional<String> text;

    private FoodNames(String text) {
        this.text = Optional.of(text);
    }

    FoodNames() {
        this.text = Optional.empty();
    }

    @Override
    public String toString() {
        return text.orElseGet(super::toString);
    }

    public Food toFood(){
        return new Food(toString());
    }
}
