package pl.edu.agh.daruljakub.bd2.generator;

import pl.edu.agh.daruljakub.bd2.BiologyClass;
import pl.edu.agh.daruljakub.bd2.dao.AilmentDAO;
import pl.edu.agh.daruljakub.bd2.dao.FoodDAO;
import pl.edu.agh.daruljakub.bd2.dao.HeroDAO;
import pl.edu.agh.daruljakub.bd2.model.Hero;
import java.util.EnumSet;

public class Generator {
    public static void generate() {
        generateHeroes();
        generateAilments();
        generateFoods();

        addSuffersFromRelationships();
        addCaresForRelationships();
        addEatsRelationships();
    }

    private static void addEatsRelationships() {
        eats(HeroNames.chris,FoodNames.birthdayCake);
        eats(HeroNames.pooh,FoodNames.honey);
        eats(HeroNames.rabbit,FoodNames.carrot);
        eats(HeroNames.roo,FoodNames.maltExtract);
        eats(HeroNames.tigger,FoodNames.maltExtract);
        eats(HeroNames.kanga,FoodNames.porridge);

        eats(HeroNames.eeyore,FoodNames.thistle);
        eats(HeroNames.piglet,FoodNames.acorn);
    }


    private static void generateHeroes(){
        generateHero(HeroNames.chris,BiologyClass.MAMAL);
        generateHero(HeroNames.pooh,BiologyClass.MAMAL);
        generateHero(HeroNames.piglet,BiologyClass.MAMAL);
        generateHero(HeroNames.tigger,BiologyClass.MAMAL);
        generateHero(HeroNames.rabbit,BiologyClass.MAMAL);
        generateHero(HeroNames.eeyore,BiologyClass.MAMAL);
        generateHero(HeroNames.kanga,BiologyClass.MAMAL);
        generateHero(HeroNames.roo,BiologyClass.MAMAL);
        generateHero(HeroNames.owl,BiologyClass.BIRD);
    }

    private static void generateAilments() {
        AilmentDAO ailmentDAO = new AilmentDAO();
        EnumSet.allOf(AilmentNames.class).forEach(ail -> ailmentDAO.addAilment(ail.toAilment()));
    }

    private static void generateFoods(){
        FoodDAO foodDAO= new FoodDAO();
        EnumSet.allOf(FoodNames.class).forEach(food-> foodDAO.addFood(food.toFood()));
    }

    private static void addSuffersFromRelationships(){
        //http://www.strongmindbraveheart.com/winnie-pooh-characters-mental-disorders/
        suffersFrom(HeroNames.pooh,AilmentNames.ocd);
        suffersFrom(HeroNames.pooh,AilmentNames.adhd);

        suffersFrom(HeroNames.piglet,AilmentNames.anxiety);

        suffersFrom(HeroNames.tigger,AilmentNames.adhd);
        suffersFrom(HeroNames.tigger,AilmentNames.adhd);

        suffersFrom(HeroNames.roo,AilmentNames.socialAnxiety);

        suffersFrom(HeroNames.rabbit,AilmentNames.ocd);

        suffersFrom(HeroNames.eeyore,AilmentNames.depression);

        suffersFrom(HeroNames.owl,AilmentNames.dyslexia);

        suffersFrom(HeroNames.chris,AilmentNames.schizophrenia);
    }

    private static void addCaresForRelationships(){
        caresFor(HeroNames.kanga,HeroNames.roo);
        caresFor(HeroNames.chris,HeroNames.pooh);
        caresFor(HeroNames.pooh,HeroNames.piglet);
    }

    private static void generateHero(HeroNames name, BiologyClass biologyClass) {
        new HeroDAO().addHero(new Hero(name.toString(),biologyClass));
    }

    private static void suffersFrom(HeroNames who, AilmentNames what){
        new HeroDAO().suffersFrom(who.toHero(),what.toAilment());
    }

    private static void eats(HeroNames who, FoodNames what){
        new HeroDAO().eats(who.toHero(),what.toFood());
    }

    private static void caresFor(HeroNames who, HeroNames forWhom){
        new HeroDAO().caresFor(who.toHero(),forWhom.toHero());
    }

}
