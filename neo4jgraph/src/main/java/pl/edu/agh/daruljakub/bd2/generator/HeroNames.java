package pl.edu.agh.daruljakub.bd2.generator;

import pl.edu.agh.daruljakub.bd2.model.Hero;

public enum HeroNames {
    chris("Christopher Robin"),
    pooh("Winnie The Pooh"),
    piglet("Piglet"),
    kanga("Kanga"),
    roo("Roo"),
    tigger("Tigger"),
    eeyore("Eeyore"),
    rabbit("Rabbit"),
    owl("Owl");


    private final String text;

    HeroNames(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }

    public Hero toHero() {
        return new Hero(toString());
    }

}
