package pl.edu.agh.daruljakub.bd2.model;

import java.util.Optional;

public class Ailment implements Named{
    public String getName() {
        return name;
    }

    public Optional<String> getShortName() {
        return shortName;
    }

    private String name;
    private Optional<String> shortName;


    public Ailment(String name){
        this.name=name;
        this.shortName=Optional.empty();
    }

    public Ailment(String name, String shortName) {
        this.name = name;
        this.shortName = Optional.of(shortName);
    }
}
