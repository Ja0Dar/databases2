package pl.edu.agh.daruljakub.bd2.model;

public class Food implements Named{
    @Override
    public String getName() {
        return name;
    }

    private final String name;
    public Food(String name) {
        this.name = name;
    }
}
