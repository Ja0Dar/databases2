package pl.edu.agh.daruljakub.bd2.model;

import pl.edu.agh.daruljakub.bd2.BiologyClass;

import java.util.Optional;

public class Hero implements Named{
    public String getName() {
        return name;
    }

    public Optional<BiologyClass> getBioClass() {
        return bioClass;
    }

    private final String name;
    private final Optional<BiologyClass> bioClass;

    public Hero(String name){
        this.name=name;
        this.bioClass =Optional.empty();
    }
    public Hero(String name, BiologyClass bioClass) {
        this.name = name;
        this.bioClass = Optional.of(bioClass);
    }

}
