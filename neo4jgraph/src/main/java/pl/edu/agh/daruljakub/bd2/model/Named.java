package pl.edu.agh.daruljakub.bd2.model;

public interface Named {
    public String getName();
}
